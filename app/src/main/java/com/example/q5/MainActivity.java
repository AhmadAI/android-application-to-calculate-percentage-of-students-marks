package com.example.q5;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q5.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button;
        EditText etxt1, etxt2, etxt3,etxt4, etxt5;
        TextView rtv;

        button=(Button)findViewById(R.id.button);
        etxt1=(EditText)findViewById(R.id.etxt1);
        etxt2=(EditText)findViewById(R.id.etxt2);
        etxt3=(EditText)findViewById(R.id.etxt3);
        etxt4=(EditText)findViewById(R.id.etxt4);
        etxt5=(EditText)findViewById(R.id.etxt5);
        rtv=(TextView)findViewById(R.id.rtv);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int s1 = Integer.parseInt(etxt1.getText().toString());
                int s2 = Integer.parseInt(etxt2.getText().toString());
                int s3 = Integer.parseInt(etxt3.getText().toString());
                int s4 = Integer.parseInt(etxt4.getText().toString());
                int s5 = Integer.parseInt(etxt5.getText().toString());

                float per = ((s1+s2+s3+s4+s5)*100)/500;

                rtv.setText(" The percentage of the above marks is: "+per);
            }
        });


    }
}